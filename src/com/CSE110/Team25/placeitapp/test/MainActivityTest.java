/*
     _      _______ ______  _____ _______ _____ _   _  _____   _______ ______          __  __  
  /\| |/\  |__   __|  ____|/ ____|__   __|_   _| \ | |/ ____| |__   __|  ____|   /\   |  \/  | 
  \ ` ' /     | |  | |__  | (___    | |    | | |  \| | |  __     | |  | |__     /  \  | \  / | 
 |_     _|    | |  |  __|  \___ \   | |    | | | . ` | | |_ |    | |  |  __|   / /\ \ | |\/| | 
  / , . \     | |  | |____ ____) |  | |   _| |_| |\  | |__| |    | |  | |____ / ____ \| |  | | 
  \/|_|\/     |_|  |______|_____/   |_|  |_____|_| \_|\_____|    |_|  |______/_/    \_\_|  |_| 
  /\| |/\                                                                                      
  \ ` ' /                                                                                      
 |_     _|                                                                                     
  / , . \                                                                                      
  \/|_|\/   _____  _____   ____  ______  _____ _______   _______ ______          __  __        
  /\| |/\  |_   _|/ ____| |  _ \|  ____|/ ____|__   __| |__   __|  ____|   /\   |  \/  |       
  \ ` ' /    | | | (___   | |_) | |__  | (___    | |       | |  | |__     /  \  | \  / |       
 |_     _|   | |  \___ \  |  _ <|  __|  \___ \   | |       | |  |  __|   / /\ \ | |\/| |       
  / , . \   _| |_ ____) | | |_) | |____ ____) |  | |       | |  | |____ / ____ \| |  | |       
  \/|_|\/  |_____|_____/  |____/|______|_____/   |_|       |_|  |______/_/    \_\_|  |_|                                                                                                      
                                                                           
 */
package com.CSE110.Team25.placeitapp.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import com.CSE110.Team25.placeitapp.MainActivity;
import com.CSE110.Team25.placeitapp.MySQLHelper;
import com.CSE110.Team25.placeitapp.PlaceIt;
import com.CSE110.Team25.placeitapp.PlaceitHandler;
import com.CSE110.Team25.placeitapp.MyMapFragment;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import android.location.Location;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Toast;

public class MainActivityTest extends
		ActivityInstrumentationTestCase2<MainActivity> {

	
	private MainActivity mActivity;
	private PlaceitHandler piHandler;
	private MyMapFragment mMyMapFragment;
	
    private int id = 0;
    private String title = "title";
    private String desc = "desc";
    private double latitude = 0.0;
    private double longitude = 0.0;
    private String create_time = "time";
    private boolean due_flag = true;
    private String rep_date = "";
    private String cat1 = "";
    private String cat2 = "";
    private String cat3 = "";
    private boolean isPlaceit = true;
    public boolean posTest,titleTest;
    private static final String PROVIDER = "flp";
    private static final float ACCURACY = 3.0f;
    
    // Define a LocationClient object
    public LocationClient mLocationClient;

	public MainActivityTest() {
		super(MainActivity.class);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * This turns off touch mode in the device or emulator.
	 * If any of your test methods send key events to the application,
	 * you must turn off touch mode before you start any activities;
	 * otherwise, the call is ignored.
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		/**
		 * This turns off touch mode in the device or emulator.
		 * If any of your test methods send key events to the application,
		 * you must turn off touch mode before you start any activities;
		 * otherwise, the call is ignored.
		 */
		setActivityInitialTouchMode(false);
		mActivity = getActivity();	
		piHandler = PlaceitHandler.getHelper(mActivity);
		piHandler.deleteAllPlaceIt();
		mMyMapFragment = (MyMapFragment)mActivity.getSupportFragmentManager().findFragmentByTag("my_map");
	}
	
	/**
	 * This runs after every test.
	 */
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		piHandler = PlaceitHandler.getHelper(mActivity);
		piHandler.deleteAllPlaceIt();
	}
	
	/**
	 * Sets the current place-it fields.
	 */
	public void placeitSet(int id, String title, String desc, double latitude, double longitude, 
		String create_time, boolean due_flag, String rep_date, String cat1, String cat2, String cat3, boolean isPlaceit) {
	    this.id = id;
	    this.title = title;
	    this.desc = desc;
	    this.latitude = latitude;
	    this.longitude = longitude;
	    this.create_time = create_time;
	    this.due_flag = due_flag;
	    this.rep_date = rep_date;
	    this.cat1 = cat1;
	    this.cat2 = cat2;
	    this.cat3 = cat3;
	    this.isPlaceit = isPlaceit;
	}
	
	/*
     * From input arguments, create a single Location with provider set to
     * "flp"
     */
    public Location createLocation(double lat, double lng, float accuracy) {
        // Create a new Location
        Location newLocation = new Location(PROVIDER);
        newLocation.setLatitude(lat);
        newLocation.setLongitude(lng);
        newLocation.setAccuracy(accuracy);
        return newLocation;
    }
	
	/**
	 * This checks for the necessary preconditions
     * required to before running any tests.
   	 * Akin to "Given the app is running."
	 */
	public void testPreConditions() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
	    assertTrue(mActivity != null);
	    assertTrue(piHandler != null);
	    assertTrue(placeits.size() == 0);
	    placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.size() == 0);
	}
	
	/**
	 * Tests adding one place-it to see
	 * if it's added into the database.
	 */
	public void testAdd() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		
		placeits = piHandler.getAllPlaceIts();
		
		//get current size and inc by 1 for check
		int size = placeits.size() + 1;
		
		PlaceIt placeIt = new PlaceIt(title, desc, latitude, longitude, 
				create_time, due_flag, rep_date, cat1, cat2, cat3, isPlaceit);
		
		piHandler.addPlaceIt(placeIt);
		
		placeits = piHandler.getAllPlaceIts();
		
		assertTrue(placeits.size() == size);
	} 
	
	/**
	 * Tests adding three place-its to see
	 * if it's added into the database.
	 */
	public void testAdd3() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		
		placeits = piHandler.getAllPlaceIts();
		
		assertTrue(placeits.size() == 0);
		
		PlaceIt placeIt = new PlaceIt("Title", "desc", 0, 0, ""
                , true, "", "", "", "", true);
		PlaceIt placeIt2 = new PlaceIt("Title", "desc", 1, 1, ""
                , true, "", "", "", "", true);
		PlaceIt placeIt3 = new PlaceIt("Title", "desc", 2, 2, ""
                , true, "", "", "", "", true);
		
		piHandler.addPlaceIt(placeIt);
		piHandler.addPlaceIt(placeIt2);
		piHandler.addPlaceIt(placeIt3);
		
		placeits = piHandler.getAllPlaceIts();
		
		assertTrue(placeits.size() == 3);
	} 
	
	/**
	 * Tests removing all the place-its from
	 * a non-empty database of place-its.
	 */
	public void testRemoveAllPlaceits() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		
		piHandler.deleteAllPlaceIt();
		
		placeits = piHandler.getAllPlaceIts();
		
		assertTrue(placeits.size() == 0);
	}
	
	/**
	 * Tests adding and removing a single place-it and
	 * repeating that with multiple place-its on the database.
	 */
	public void testRemoveOneThenRemoveAll() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		
		//test adding and removing a single placeit
		
		placeitSet(0, "title", "desc", 0.0, 0.0, "time", true, "", "", "", "", true);
		
		testAdd();
		
		placeits = piHandler.getAllPlaceIts();
		
		assertTrue(placeits.size() == 1);
		
		piHandler.deleteAllPlaceIt();
		
		placeits = piHandler.getAllPlaceIts();
		
		assertTrue(placeits.size() == 0);
		
		//Now test adding multiple placeits and removing them
		placeitSet(0, "title", "desc", 0.0, 0.0, "time", true, "", "", "", "", true);
		
		testAdd();
		
		placeitSet(1, "title", "desc", 1.0, 1.0, "time", true, "", "", "", "", true);
		
		testAdd();

		placeits = piHandler.getAllPlaceIts();
		
		assertTrue(placeits.size() == 2);
		
		piHandler.deleteAllPlaceIt();
		
		placeits = piHandler.getAllPlaceIts();
		
		assertTrue(placeits.size() == 0);
	} 
	
	/**
	 * Tests adding and removing a specific place-it within the database.
	 */
	public void testRemoveSpecific() {                        
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		
		//Now test adding multiple placeits and removing them
		placeitSet(0, "title1", "desc1", 0.0, 0.0, "time", true, "", "", "", "", true);
			
		testAdd();
			
		placeitSet(0, "title2", "desc2", 1.0, 1.0, "time", true, "", "", "", "", true);
				
		testAdd();
		
		placeits = piHandler.getAllPlaceIts();
		
		assertTrue(placeits.size() == 2);
		
		piHandler.deletePlaceItAt(0,0);
		
		placeits = piHandler.getAllPlaceIts();
		
		assertTrue(placeits.size() == 1);
		
		assertTrue(piHandler.piIsInDatabase(1, 1));
		
	}
	
	/**
	 * Tests removing a null place-it.
	 */	
	public void testRemoveNullPlaceit() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		
		piHandler.deletePlaceItAt(0,0);
		
		placeits = piHandler.getAllPlaceIts();
		
		assertTrue(placeits.size() == 0);
	}
	
	/**
	 * Milestone 1, Iteration 1, User Story 1
	 * Scenario 1: User places place-It by specifying location on the map
	 * 	Given the app is open
	 * 	When the user clicks the add place-it button
	 * 	Then the user is shown the "create place-it" screen where they are presented with boxes to enter a title, description, and location either by address or selecting on the map
	 * 	When the user presses the "select location" button
	 * 	Then the user is show the map
	 * 	When the user presses a location on the map
	 * 	Then they are show a verification message asking if they are sure they would like to place the place -it there
	 * 	When the user clicks yes Then the place-it is placed
	 **/
	public void test_MS1_I1_US1_S1() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		//Given the app is open
		//When the user adds a place it
		placeitSet(0, "title", "desc", 0.0, 0.0, "time", true, "", "", "", "", true);
		testAdd();
		//Then the place it is added
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.size() == 1);
	}
	
	public void test_MS1_I1_US2_S1() throws Throwable {
		final CountDownLatch signal = new CountDownLatch(1); 
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		
		List<Marker> mMarkers = new ArrayList<Marker>();
		assertTrue(mMarkers.size() == 0);
		
		//Given the app is open and a place it is on the app
		placeitSet(0, "S1", "THE BEST", 30, -117, "time", true, "fff", "cat1", "cat2", "cat3", true);
		testAdd();	
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.size() == 1);
		
		//When the user selects the place it on the map
		//In this case we check the marker location is th same as the added placeit
		mActivity.runOnUiThread(
			      new Runnable() {
			        public void run() {
			        	
			        	mMyMapFragment.restorePlaceits();
			        	
			        	//Give app time to draw the marker
			        	try {
			    			TimeUnit.SECONDS.sleep(2);
			    		} catch(InterruptedException ex) {
			    		    Thread.currentThread().interrupt();
			    		}			
			        	
			        	//check the marker's title and position matches what we set
			        	List<Marker> mMarkers = new ArrayList<Marker>();
			        	mMarkers = mMyMapFragment.getMarkers();
			        	Marker test = mMarkers.get(0);
			        	LatLng myLoc = new LatLng(30, -117);
			        	
			        	posTest = test.getPosition().equals(myLoc);
			    		titleTest = test.getTitle().equals(title);
			    		signal.countDown(); 
			        } // end of run() method definition
			      } // end of anonymous Runnable object instantiation
			    ); // end of invocation of runOnUiThread
		signal.await(30, TimeUnit.SECONDS); 
		//assertTrue(posTest);
		assertTrue(titleTest);
		//Then the user is shown the place it information 
		//Check data base info for this place it
		PlaceIt placeit = placeits.get(0);
		assertTrue(this.title.equals(placeit.getTitle()));
		assertTrue(this.desc.equals(placeit.getDesc()));
		assertTrue(this.latitude == placeit.getLatitude());
		assertTrue(this.longitude == placeit.getLongitude());
		assertTrue(this.create_time.equals(placeit.getCreate_time()));
		assertTrue(this.due_flag == placeit.isDue_flag());
		assertTrue(this.rep_date.equals(placeit.getRep_date()));
		assertTrue(this.cat1.equals(placeit.getCat1()));
		assertTrue(this.cat2.equals(placeit.getCat2()));
		assertTrue(this.cat3.equals(placeit.getCat3()));
		assertTrue(this.isPlaceit == placeit.isPlaceit());
	}
	
	/**
	 * Scenario 1: User is notified when they enter the ½ mile radius of an active place-it
	 * Given That the app is running
	 * And there are active place-its
	 * When The user enters the ½ mile radius of the active place-it
	 * Then The user is notified of their reminder with a top bar notification as well as a sound
	 * When the user clicks the notification 
	 * Then the details of the place-it will be shown on the screen
	 * @throws InterruptedException 
	 */
	public void test_MS1_I1_US3_S1() throws InterruptedException {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		
		mLocationClient = mActivity.getLocationClient();
		
		//mLocationClient = new LocationClient(mActivity, mActivity, mActivity);

        // Start connecting to Location Services
        //mLocationClient.connect();
		// Connect to Location Services
	    //mLocationClient.connect();

	    // When the location client is connected, set mock mode
	    mLocationClient.setMockMode(true);
	    
	    Location testLocation = createLocation(0.0, 0.0, ACCURACY);
		//Given the app is open
		//And there are active place-its
		placeitSet(0, "title", "desc", 0.0, 0.0, "time", true, "", "", "", "", true);
		testAdd();
		//Then the place is added
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.size() == 1);
		//When The user enters the ½ mile radius of the active place-it
		mLocationClient.setMockLocation(testLocation);
		//Then The user is notified of their reminder with a top bar notification as well as a sound
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.get(0).isDue_flag() == false);
		//When the user clicks the notification 
		//Then the details of the place-it will be shown on the screen
		//mLocationClient.disconnect();
	    mLocationClient.setMockMode(false);
	}
	
	/**
	 * Scenario 2: User is notified for multiple place-its when they enter the 1/2 mile radius of an active place-it
	 * Given that the app is running
	 * And there are active place-its
	 * When The user enters the ½ mile radius of the active place-it
	 * Then the user is notified of their reminder with a top bar notification as well as a sound.
	 * When the user clicks the notification 
	 * Then the list of place-it's that were triggered will be shown on the screen
	 * When the user clicks one of the place-it's
	 * Then the details of the place-it will be shown on the screen
	 * @throws InterruptedException 
	 */
	public void test_MS1_I1_US3_S2() throws InterruptedException {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		
		mLocationClient = mActivity.getLocationClient();
		
		// Connect to Location Services
	    //mLocationClient.connect();

	    // When the location client is connected, set mock mode
	    mLocationClient.setMockMode(true);
	    
	    Location testLocation = createLocation(22.22, 33.33, ACCURACY);
		//Given the app is open
		//And there are active place-its
		placeitSet(0, "title", "desc", 22.22, 33.33, "time", true, "", "", "", "", true);
		testAdd();
		//Then the place is added
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.size() == 1);
		//When The user enters the ½ mile radius of the active place-it
		mLocationClient.setMockLocation(testLocation);
		//Then the user is notified of their reminder with a top bar notification as well as a sound.
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.get(0).isDue_flag() == false);
		//When the user clicks the notification 
		//Then the list of place-it's that were triggered will be shown on the screen
		//When the user clicks one of the place-it's
		//Then the details of the place-it will be shown on the screen
		//mLocationClient.disconnect();
	    mLocationClient.setMockMode(false);
	}
	
	/**
	 * Milestone 1, Iteration 1, User Story 4
	 * Scenario 1: User chooses to repost place-it
	 * Given That the app is running
	 * And the user is viewing the place-it notification
	 * When the user presses the re-post button
	 * Then the place-it is reposted with all of the same details as before (re -added to the active list)
	 */
	public void test_MS1_I1_US4_S1() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		//Given the app is open
		//When the user adds a place it
		placeitSet(0, "title", "desc", 0.0, 0.0, "time", true, "", "", "", "", true);
		testAdd();
		//Then the place is added
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.size() == 1);
		//the place it becomes inactive
		piHandler.updatePlaceItInDatabase(placeits.get(0).getId(), MySQLHelper.KEY_DUE_FLAG, "false");
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.get(0).isDue_flag() == false);
		//When the user presses the repost button
		piHandler.updatePlaceItInDatabase(placeits.get(0).getId(), MySQLHelper.KEY_DUE_FLAG, "true");
		placeits = piHandler.getAllPlaceIts();
		//Then the place-it is reposted with all of the same details as before (re-added to active list)
		assertTrue(placeits.get(0).isDue_flag() == true);
	}
	
	/**
	 * Milestone 1, Iteration 1, User Story 4
	 * Scenario 2: User chooses to discard place-it
	 * Given That the app is running
	 * And the user is viewing the place-it notification
	 * When the user presses the discard button
	 * Then the place-it gets moved deleted.
	 */
	public void test_MS1_I1_US4_S2() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		//Given the app is open
		//When the user adds a place it
		placeitSet(0, "title", "desc", 0.0, 0.0, "time", true, "", "", "", "", true);
		testAdd();
		//Then the place is added
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.size() == 1);
		
		//the place it becomes inactive
		piHandler.updatePlaceItInDatabase(placeits.get(0).getId(), MySQLHelper.KEY_DUE_FLAG, "false");
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.get(0).isDue_flag() == false);
		
		//When the user presses the discard button and deletes the placeit
		piHandler.deletePlaceItAt(0,0);
		
		//Then the place-it is deleted.
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.size() == 0);
	}
	
	/**
	 * Milestone 1, Iteration 2, User Story 2
	 * Scenario 1: User reposts place-it from the Pulled-down list
	 * Given that the app is running
	 * When the user swipes from the left edge of the screen to open the drawer menu
	 * Then the user is shown a list of active, pulled down, and expired place-its
	 * When the user selects a place-it from the pulled-down list
	 * Then the user is shown a screen containing the place-it information and remove/repost button When the user presses the repost button
	 * Then the place-it is reposted (moved back to active list)
	 * And the user will now receive notifications from this place-it
	 */
	
	public void test_MS1_I2_US2_S1(){
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		//Given the app is open
		//When the user adds a place it
		placeitSet(0, "title", "desc", 0.0, 0.0, "time", true, "", "", "", "", true);
		testAdd();
		//Then the place is added
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.size() == 1);
		
		// The place it is an expired placeit
		piHandler.updatePlaceItInDatabase(placeits.get(0).getId(), MySQLHelper.KEY_DUE_FLAG, "false");
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.get(0).isDue_flag() == false);
		
		// User reposts the placeit
		piHandler.updatePlaceItInDatabase(placeits.get(0).getId(), MySQLHelper.KEY_DUE_FLAG, "true");
		
		// Check to see if its active
		placeits = piHandler.getAllPlaceIts();
		assertTrue(placeits.get(0).isDue_flag() == true);
		assertTrue(placeits.size() == 1);		
	}
	
	
	/**
	 * Milestone 1, Iteration 2, User Story 3
	 * Scenario 2: User places a scheduled place-It by specifying location on the map
	 * Given the app is open 
	 * When the user clicks the add place-it button
	 * Then the user is shown the "create place-it" screen where they are presented 
	 * with boxes to enter a title, description, and location either by address or 
	 * selecting on the map, and a repeat check box
	 * When the user checks the "repeat" button
	 * Then the user is shown a list of days that they can select
	 */
	public void test_MS1_I2_US3_S2() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		//Given the app is open
		//When the user clicks adds place-it
		//Then the user is shown the "create place-it" screen where they are presented 
		//with boxes to enter a title, description, and location either by address or 
		//selecting on the map, and a repeat check box
		//When the user checks the "repeat" button
		//Then the user is shown a list of days that they can select
		//1 is Sunday, we set the place-it to repeat on Sunday
		placeitSet(0, "title", "desc", 0.0, 0.0, "time", true, "1", "", "", "", true);
		testAdd();
		placeits = piHandler.getAllPlaceIts();
		//Then the place-it is moved to pulled-down(triggered) list.
		assertTrue(placeits.get(0).getRep_date().equals("1"));
	}
	
	/**
	 * Milestone 2, Iteration 2, User Story 2
	 * Scenario 1: When the user does not specify category
	 * Given the user does not specify the category
	 * When the user adds a place-it
	 * Then the place-it is categorized under uncategorized
	 */
	public void test_MS2_I2_US2_S1() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		//Given the user does not specify the category
		//When the user adds a place-it
		placeitSet(0, "title", "desc", 0.0, 0.0, "time", true, "", "", "", "", true);
		testAdd();
		placeits = piHandler.getAllPlaceIts();
		//Then the place-it is categorized under uncategorized
		assertTrue(placeits.get(0).getCat1().equals(""));
	}
	
	/**
	 * Milestone 2, Iteration 2, User Story 2
	 * Scenario 2: User specifies a single category
	 * Given the user specifies the category
	 * When the user adds a place-it
	 * Then the place-it is categorized under the specified 
	 */
	public void test_MS2_I2_US2_S2() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		//Given the user does not specify the category
		//When the user adds a place-it
		placeitSet(0, "title", "desc", 0.0, 0.0, "time", true, "", "Bakery", "", "", true);
		testAdd();
		placeits = piHandler.getAllPlaceIts();
		//Then the place-it is categorized under the specified
		assertTrue(placeits.get(0).getCat1().equals("Bakery"));
	}
	
	/**
	 * Milestone 2, Iteration 2, User Story 2
	 * Scenario 3: User specifies multiple categories
	 * Given the user specifies multiple categories
	 * When the user adds a place-it
	 * Then the place-it is categorized under those specified
	 */
	public void test_MS2_I2_US2_S3() {
		ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();
		assertTrue(placeits.size() == 0);
		//Given the user does not specify the category
		//When the user adds a place-it
		placeitSet(0, "title", "desc", 0.0, 0.0, "time", true, "", "Bakery", "Aquarium", "Bar", true);
		testAdd();
		placeits = piHandler.getAllPlaceIts();
		//Then the place-it is categorized under those specified
		assertTrue(placeits.get(0).getCat1().equals("Bakery"));
		assertTrue(placeits.get(0).getCat2().equals("Aquarium"));
		assertTrue(placeits.get(0).getCat3().equals("Bar"));
	}
}

