/*
     _      _______ ______  _____ _______ _____ _   _  _____   _______ ______          __  __  
  /\| |/\  |__   __|  ____|/ ____|__   __|_   _| \ | |/ ____| |__   __|  ____|   /\   |  \/  | 
  \ ` ' /     | |  | |__  | (___    | |    | | |  \| | |  __     | |  | |__     /  \  | \  / | 
 |_     _|    | |  |  __|  \___ \   | |    | | | . ` | | |_ |    | |  |  __|   / /\ \ | |\/| | 
  / , . \     | |  | |____ ____) |  | |   _| |_| |\  | |__| |    | |  | |____ / ____ \| |  | | 
  \/|_|\/     |_|  |______|_____/   |_|  |_____|_| \_|\_____|    |_|  |______/_/    \_\_|  |_| 
  /\| |/\                                                                                      
  \ ` ' /                                                                                      
 |_     _|                                                                                     
  / , . \                                                                                      
  \/|_|\/   _____  _____   ____  ______  _____ _______   _______ ______          __  __        
  /\| |/\  |_   _|/ ____| |  _ \|  ____|/ ____|__   __| |__   __|  ____|   /\   |  \/  |       
  \ ` ' /    | | | (___   | |_) | |__  | (___    | |       | |  | |__     /  \  | \  / |       
 |_     _|   | |  \___ \  |  _ <|  __|  \___ \   | |       | |  |  __|   / /\ \ | |\/| |       
  / , . \   _| |_ ____) | | |_) | |____ ____) |  | |       | |  | |____ / ____ \| |  | |       
  \/|_|\/  |_____|_____/  |____/|______|_____/   |_|       |_|  |______/_/    \_\_|  |_|                                                                                                      
                                                                           
 */

package com.CSE110.Team25.placeitapp.test;

import com.CSE110.Team25.placeitapp.MainActivity;
import com.CSE110.Team25.placeitapp.PlaceitHandler;
import com.robotium.solo.Solo;

import android.test.ActivityInstrumentationTestCase2;

public class ScenarioBasedTest extends
		ActivityInstrumentationTestCase2<MainActivity> {
	
	private Solo solo;
	private PlaceitHandler piHandler;
	private MainActivity mActivity;
	
	public ScenarioBasedTest() {
		super(MainActivity.class);
		// TODO Auto-generated constructor stub
	}

	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());
		mActivity = getActivity();	
		piHandler = PlaceitHandler.getHelper(mActivity);
		piHandler.deleteAllPlaceIt();
		
	}
	
	/**
	 * This runs after every test.
	 */
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		piHandler = PlaceitHandler.getHelper(mActivity);
		piHandler.deleteAllPlaceIt();
	}
	
	public void testAdd(){
		solo.assertCurrentActivity("Check on first activity", MainActivity.class);
		solo.sleep(4000);
		solo.clickOnScreen(640, 930);
        
        assertTrue(solo.searchText("Add"));
        //solo.sleep(100);
        
        solo.clearEditText(0);
         
        solo.enterText(0, "stest");
         
        //solo.sleep(100);
        
        solo.clickOnButton("Add");
        solo.sleep(1000);
        
        
        solo.clickOnScreen(640, 930);
         
        solo.sleep(100);
         
        solo.clickOnScreen(960, 480);
         
        solo.sleep(100);
         
        assertTrue(solo.searchText("stest"));
	
	}
	
	/**
	 * Scenario 1: User discard place-it (moves place-it from active-list to pull-down list) Given that the app is running
	 * When the user swipes from the left edge of the screen to open the drawer menu Then the user is shown a list of active, pulled down, and expired place-its
	 * When the user selects a place-it from the active list
	 * Then the user is shown a screen containing the place-it information When the user presses the discard button
	 * Then the place-it is moved to the pulled-down list
	 * And the user will not receive any more notifications from this place -it
	 */
	public void test_MS1_I2_US1_S1(){
		// Given that the app is running
		solo.assertCurrentActivity("Check on first activity", MainActivity.class);
		solo.sleep(4000);
		// And a marker exists
		solo.clickOnScreen(640, 930);
        assertTrue(solo.searchText("Add"));
        solo.clearEditText(0);
        solo.enterText(0, "test");
        solo.clickOnButton("Add");
        solo.sleep(1000);
        
		// When the user pressed the button to access the list of place its
        solo.clickOnText("List");
		
        // And deletes a place it
		assertTrue(solo.searchText("test"));
		solo.clickOnText("test");
		assertTrue(solo.searchText("Delete?"));
		solo.clickOnButton("Delete?");
		
		// The placeit will be removed from the list and the user will no longer be notified
		solo.clickOnText("List");
		assertFalse(solo.searchText("test"));
	}

}
